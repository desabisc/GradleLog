package mx.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradleLogApplication.class, args);
	}

}
