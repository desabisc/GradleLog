package mx.com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class Service {

	@GetMapping("/")
	public String index() {
		for (int i = 0; i <= 5; i++) {
			log.info("Mensaje " + i);
		}
		log.info("###############################################");
		return "logging test";
	}
	
}
